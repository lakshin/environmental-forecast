## todo list


Things that was asked to be rectified in this paper are :

    Correcting the text in paper(Bigger Intro(an expansion of abstract), remove problem statement section, add to intro, make more citations. Everything that was not part of our contribution has to be cited, Every experiment has to be repeated with 3 datasets or biggest superset of available dataset, every expt has to be tried out for 4-5 models like Log. Reg, exaboost but after my research specific to time series-ARIMA,SARIMA,adaboost, adaboost-lstm random forests, XGBoost, models can be used. add captions to the figures and graphs and clearly explain what is given in them, export all images as EPS format and use epstopdf package in latex. all figures and tables must be centered. describe and explain why these tests and what they infer like adf, kpss tests. add more references. ADD One more parameter for Forest Cover Prediction. Change visualization as necessary to make explanation simpler.
    Data Preprocessing finding for 4 factors - land avg temperature, sea level, air quality, forest cover all time series(fundamental approach in our paper) Justify why we went with only time series data.(adv of time series data)
    Model building and Need to get some sort of metric like accuracy for each model. Then compare between models. and justify the results.
    Make more graphs and similar graphs. and Explain them.

Attached files: Project Report, Airquality.ipynb, 2 datasets, 2 R files 



Checklist:


    * Research paper analysis. more references. even sarima, kpss test etc.   more pending
    * Datasets Analysis. and Preprocessing                                  
    * Finalize the experimental setup with model names.                     
    * Start Fixing the initial mistakes of the paper sir had mentioned.     
    * Implement the code as necessary                                       
    * Visualize the code.                                                   
    * Final Comparison and justification study and writing and Citation.    
    * Send email to keshab nath sir.                                        